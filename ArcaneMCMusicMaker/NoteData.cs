﻿using System;
using System.Collections;
using Newtonsoft.Json;

namespace ArcaneMCMusicMaker
{
    public class NoteData
    {
        public Instrument instrument = Instrument.PIANO;
        public Note note = new Note();
    }
    public class Note
    {
        public int note = 10;
    }
    public enum Instrument
    {
        PIANO,
        BASS_DRUM,
        BASS_GUITAR,
        BELL,
        CHIME,
        FLUTE,
        GUITAR,
        SNARE_DRUM,
        STICKS,
        XYLOPHONE
    }

}