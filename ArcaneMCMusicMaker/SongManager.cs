﻿using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ArcaneMCMusicMaker
{
    public class SongManager
    {
        public static SongData CurrentSong = new SongData();
        public static void LoadFile(string path)
        {
            CurrentSong = JsonConvert.DeserializeObject<SongData>(File.ReadAllText(path));
            Debug.WriteLine(JsonConvert.SerializeObject(CurrentSong, Formatting.Indented, new StringEnumConverter()));
        }
    }
}