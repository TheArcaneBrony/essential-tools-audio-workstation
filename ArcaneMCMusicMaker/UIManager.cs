﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace ArcaneMCMusicMaker
{
    public class UIManager
    {
        public static ContextMenu FileMenu = new ContextMenu();
        public static SolidColorBrush White = new(Colors.White), DarkGray = new(Colors.DarkSlateGray), Test = new(Colors.Indigo);

        public static void Init()
        {
            MenuItem File_OpenFile = new MenuItem() { Header = "Open file..." };
            File_OpenFile.Click += (object sender, RoutedEventArgs e) =>
            {
                OpenFileDialog ofd = new OpenFileDialog()
                {
                    Multiselect = false,
                    Title = "Open a music json",
                    InitialDirectory = "D:\\Spigot Test"
                };
                bool? fileselected = ofd.ShowDialog();
                if (fileselected.HasValue && fileselected.Value)
                {
                    SongManager.LoadFile(ofd.FileName);
                }
            };
            FileMenu.Items.Add(File_OpenFile);
        }
        public static void InitBaseUI(StackPanel WindowContent)
        {
            foreach (Instrument _instr in (Instrument[])Enum.GetValues(typeof(Instrument)))
            {
                StackPanel instrumentRow = new();
                Label label = new Label() { Content = _instr.ToString(), Foreground = White };
                instrumentRow.Children.Add(label);
                
                StackPanel _row = new(){Orientation = Orientation.Horizontal};
                for (int j = 0; j < 20; j++)
                {
                    Grid grid = new Grid()
                    {
                        Width=48, Height=22, Background = Test
                    };
                    grid.Children.Add(new Label(){Content=j});
                    Border border = new() {BorderBrush = DarkGray, BorderThickness = new Thickness(1,1,1,1), Child = grid};
                    _row.Children.Add(border);
                }
                
                instrumentRow.Children.Add(_row);
                for (int i = 0; i < 3; i++)
                {
                    StackPanel noteStack = new(){Orientation = Orientation.Horizontal};
                    for (int j = 0; j < 20; j++)
                    {
                        Grid grid = new Grid()
                        {
                            Width=48, Height=48, Background = Test
                        };
                        grid.Children.Add(new Label(){Content=i+""+j});
                        Border border = new() {BorderBrush = DarkGray, BorderThickness = new Thickness(1,1,1,1), Child = grid};
                        noteStack.Children.Add(border);
                    }
                    instrumentRow.Children.Add(noteStack);
                }
                
                //InstrumentRows.Add(_instr, instrumentRow);
                WindowContent.Children.Add(instrumentRow);
                
                WindowContent.Children.Add(new Line()
                {
                    Fill=DarkGray,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    Stroke=DarkGray,
                    StrokeThickness = 2,
                    X1=0,
                    X2=500,
                    Y1=0,
                    Y2=0,
                    Width = 500
                });
            }
        }
    }
}