﻿using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ArcaneMCMusicMaker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void FileMenuLabel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            UIManager.FileMenu.IsOpen ^= true;
        }

        public Dictionary<Instrument, StackPanel> InstrumentRows = new();
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WindowBlur.EnableBlur(this);
            UIManager.InitBaseUI(WindowContent);
            
            
            SongData nsd = new SongData();
            Random rnd = new Random();
            nsd.Name = "Test song";
            nsd.TickDelay = 1;
            Dictionary<long, List<NoteData>> nsdn = new();
            string[] tones = "A B C D E F G".Split(" ");
            string[] instr = "PIANO BASS_DRUM BASS_GUITAR BELL CHIME FLUTE GUITAR SNARE_DRUM STICKS XYLOPHONE".Split(" ");
            int SongTicks = 400;
            for (int j = 0; j < SongTicks; j++)
            {
                nsdn.Add((long)j, new List<NoteData>());
            }
            for (int j = 0; j < SongTicks; j++)
            {
                for (int k = 0; k < rnd.Next(10); k++)
                {
                    nsdn[(long)j].Add(new NoteData()
                    {
                        instrument = (Instrument)Enum.Parse(typeof(Instrument), instr[(int)(rnd.Next(instr.Length))]),
                        note = new Note() { note = 10 }
                    });
                }
            }
            nsd.Notes = nsdn;



           
        }

        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MinimiseButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
        bool MaximiseState = false;
        private void MaximiseButton_Click(object sender, RoutedEventArgs e)
        {
            MaximiseState ^= true;
            WindowState = MaximiseState ? WindowState.Maximized : WindowState.Normal;
        }
    }
}
