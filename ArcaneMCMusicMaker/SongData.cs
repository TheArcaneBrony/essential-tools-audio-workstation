﻿using System.Collections.Generic;

namespace ArcaneMCMusicMaker
{
    public class SongData
    {
        public string Name = "Untitled";
        public long TickDelay = 1;
        public Dictionary<long, List<NoteData>> Notes = new();
    }
}